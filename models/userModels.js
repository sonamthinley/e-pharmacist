const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: [true, 'Please tell us your name!'],
    },
    email: {
        type: String, 
        required: [true, 'Please provide your email!'],
        unique: true,
        lowercase: true, 
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo: {
        type: String, 
        default: 'default.jpg',
    },
    role: {
        type: String, 
        enum: ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user',
    },
    password: {
        type: String,
        required: [true, 'Please provide a password!'],
        minlength: 8,
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please Confirm your password'],
        validate:{
            //This only works on SAVE!!!
            validator: function (el) {
                return el === this.password
            },
            message: 'Passwords are not same',
        },
    },
})

userSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next()

    //hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12)
    //Delete passwordConfirm field 
    this.passwordConfirm = undefined
    next()
})

userSchema.methods.correctPassword = async function (
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User

const bcrypt = require('bcryptjs')